FROM node:10-alpine

LABEL name = "Kyoko Gateway"
LABEL version = "3.0.0"
LABEL maintainer = "Alula <gabixdeveloper@gmail.com>"

WORKDIR /opt/kyoko/

COPY package.json yarn.lock ./

RUN apk add --update \
&& apk add --no-cache ca-certificates \
&& apk add --no-cache --virtual .build-deps git curl build-base python g++ make \
&& yarn install \
&& apk del .build-deps g++ make python build-base git curl

COPY src resources i18n LICENSE README.md ./

RUN yarn build && yarn install --production && yarn cache clean

CMD ["node", "dist/index.js"]
