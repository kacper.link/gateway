import { MongoDBCache } from "./MongoDBCache";
import {MongoClient} from "mongodb";
import dotenv from "dotenv";

dotenv.config();

(async () => {
    const mongo = await MongoClient.connect(process.env.MONGO_DSN, {useNewUrlParser: true});
    if (!mongo) {
        throw new Error("Failed connecting to Mongo.");
    }

    const mongoDB = mongo.db(process.env.MONGO_DB);
    if (!mongoDB) {
        throw new Error("Provided mongo database doesn't exists.");
    }

    const cache = new MongoDBCache(mongoDB);

    console.log(await cache.getGuild(process.argv[2]));
})();
