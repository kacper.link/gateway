import { Redis } from "ioredis";
import { Shard } from "./Shard";
import { logger } from "./utils/Logger";

export class DistributedShardQueue {
    private readonly redis: Redis;
    private shards: Shard[];
    private polling: boolean;

    constructor(redis: Redis) {
        this.redis = redis;
        this.shards = [];
        this.polling = false;
    }

    public queue(shard: Shard) {
        if (this.shards.includes(shard)) {
            logger.warning("Ignoring duplicate queue for shard " + shard.id);
            return;
        }

        this.shards.push(shard);
        this.poll();
    }

    private poll() {
        if (this.polling) return;
        this.polling = true;
        //setTimeout(this.startShard.bind(this), 5500);
        process.nextTick(this.startShard.bind(this));
    }

    private async startShard() {
        if (this.shards.length === 0) {
            this.polling = false;
            return;
        }

        let nextStart = parseInt(await this.redis.get("shard:next_start"));
        if (!isNaN(nextStart) && (nextStart > Date.now())) {
            console.log(nextStart, Date.now());
            setTimeout(this.startShard.bind(this), 5100);
            return;
        }

        const shard = this.shards.shift();
        if (shard.socket && shard.socket.readyState == 1) {
            //we must close active connection to avoid reconnect loop [tested] (shard will add himself to queue on close)

            shard.socket.close(1001);
            await shard.clearSession();  // idk why, i can't resume after this close ;/
            setTimeout(this.startShard.bind(this), 300);
            return;
        }

        logger.info(`Starting shard ${shard.id}/${shard.max-1}...`);

        await this.redis.set("shard:next_start", Date.now() + 5100);

        if (!(await shard.checkSession())) {
            logger.debug("Invalidating session on shard " + shard.id + ".");
            await shard.clearSession();
        }

        await shard.start();

        setTimeout(this.startShard.bind(this), 5100);
    }
}
