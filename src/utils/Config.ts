import crypto from "crypto";
import { logger } from "./Logger";

export const validateConfig = () => {
    if (!process.env.KP_KEY) {
        console.log("Generated KP_KEY: ", crypto.randomBytes(64).toString("hex"));
        throw "API Key protection key (KP_KEY) is not defined!";
    }

    if (!process.env.REDIS_DSN) {
        throw "Redis connection DSN (REDIS_DSN) is not defined!";
    }

    if (!process.env.MONGO_DSN) {
        throw "Mongo connection DSN (MONGO_DSN) is not defined!";
    }

    if (!process.env.MONGO_DB) {
        throw "Mongo database name (MONGO_DB) is not defined!";
    }

    if (!process.env.SINGYEONG_DSN) {
        throw "Singyeong connection DSN (SINGYEONG_DSN) is not defined!";
    }

    if (!process.env.INSTANCE_TYPE) {
        logger.warning("Instance type (INSTANCE_TYPE) is not defined, falling back to `dev`!");
        process.env.INSTANCE_TYPE = "dev";
    }
};
