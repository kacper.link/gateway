import { createLogger, format, LeveledLogMethod, LogMethod, transports } from "winston";

const fmt = format.combine(
    format.colorize(),
    format.timestamp({
        format: "HH:mm:ss.SSS"
    }),
    format.printf(info => `[${info.timestamp}] [${info.level}] ${info.message}`)
);

export const logger = createLogger({
    format: fmt,
    level: "debug",
    transports: [
        new transports.Console()
    ]
});

class LoggerWrapper {
    private info: LeveledLogMethod;
    private warn: LeveledLogMethod;
    private error: LeveledLogMethod;
    private debug: LeveledLogMethod;
    private trace: LeveledLogMethod;
    private log: LogMethod;

    constructor() {
        this.info = logger.info;
        this.warn = logger.warning;
        this.error = logger.error;
        this.debug = logger.debug;
        this.trace = logger.debug;
        this.log = logger.log;
    }

    static get() {
        return new LoggerWrapper();
    }
}

export const logError = (e) => {
    logger.error(e);
    if (e.stack) {
        logger.error(e.stack);
    }
    if (e.cause) {
        logError(e.cause);
    }
};

export const hookLogger = () => {
    const loggerAPI = require("@ayana/logger-api");

    loggerAPI.Logger = LoggerWrapper;
    loggerAPI.default = LoggerWrapper;
};
