import { SingyeongClient } from "singyeong-client";
import dotenv from "dotenv";
import IORedis from "ioredis";
import fs from "fs";
import crypto from "crypto";
import { validateConfig } from "./utils/Config";
import { logError, logger } from "./utils/Logger";
import { decryptKey } from "./utils/KeyCrypt";
import { Shard } from "./Shard";
import { DistributedShardQueue } from "./DistributedShardQueue";
import { MongoClient } from "mongodb";

dotenv.config();

logger.info(" __                 __           __           __                           ");
logger.info("|  |--.--.--.-----.|  |--.-----.|  |--.-----.|  |_   .--------.-----.-----.");
logger.info("|    <|  |  |  _  ||    <|  _  ||  _  |  _  ||   _|__|        |  _  |  -__|");
logger.info("|__|__|___  |_____||__|__|_____||_____|_____||____|__|__|__|__|_____|_____|");
logger.info("      |_____|                                                              ");
logger.info("");

(async () => {
    validateConfig();
    if (!fs.existsSync("storage")) { // meh
        await fs.promises.mkdir("storage");
    }

    logger.info("Initializing Kyoko gateway service, instance type = " + process.env.INSTANCE_TYPE);

    if (process.env.SINGYEONG_DSN !== "DISABLE") {
        const singyeong = new SingyeongClient(process.env.SINGYEONG_DSN, {
            clientId: crypto.randomBytes(8).toString("hex"),
            applicationId: "gateway"
        });
        await singyeong.connect();
        logger.info("Connected to 신경.");
    }

    const redis = new IORedis(process.env.REDIS_DSN);
    await redis.ping();
    logger.info("Connected to Redis.");

    const mongo = await MongoClient.connect(process.env.MONGO_DSN, {useNewUrlParser: true});
    if (!mongo) {
        throw new Error("Failed connecting to Mongo.");
    }
    logger.info("Connected to Mongo.");

    const mongoDB = mongo.db(process.env.MONGO_DB);
    if (!mongoDB) {
        throw new Error("Provided mongo database doesn't exists.");
    }


    const queue = new DistributedShardQueue(redis);

    let token = await redis.get(`apikey:${process.env.INSTANCE_TYPE}:discord`);
    if (!token) {
        throw "No Discord token specified! (key = `discord`)";
    }
    token = decryptKey(token);

    logger.info("Fetched Discord token, starting the shards...");
    for (let i = 0; i < 6; i++) {
        queue.queue(new Shard(redis, mongoDB, queue, token, i, 12));
    }
})().catch(e => {
    logger.error("Oops, we did a fucky wucky uwu");
    logError(e);
    process.exit(1);
});
