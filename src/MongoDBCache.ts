import {Channel, Guild, Member, User} from "./Payload";
import {Collection, Db} from "mongodb";
import {Status} from "./Constants";

interface CachePresence extends Member { // EVENTS: Presence Update & Member & cache_guild()
    status: Status,
    guild_id: string
}

interface CacheGuild extends Guild {
    member_ids: string[];
    channel_ids: string[];
}

function clone(obj: Object) {
    return JSON.parse(JSON.stringify(obj));
}

export class MongoDBCache {
    private readonly mongoDB: Db;

    private readonly channelCache: Collection;
    private readonly guildCache: Collection;
    private readonly userCache: Collection;
    private readonly presenceCache: Collection;

    constructor(mongoDB: Db) {
        this.mongoDB = mongoDB;

        this.channelCache = mongoDB.collection("channels");
        this.guildCache = mongoDB.collection("guilds");
        this.userCache = mongoDB.collection("users");
        this.presenceCache = mongoDB.collection("presences");
    }

    private prepare(obj_: Object) {
        let obj = clone(obj_);
        Object.keys(obj).forEach(k => (typeof obj[k] == "undefined" || k == "id") && delete obj[k]);
        return obj;
    }

    public async bulkCacheGuilds(guilds: Guild[]) {
        if (!guilds) return;

        for (const guild of guilds) {
            await this.cacheGuild(guild);
        }
    }

    public async cacheChannel(channel: Channel) {
        if (channel) {
            let strippedChannel: Channel = {
                id: channel.id,
                type: channel.type,
                guild_id: channel.guild_id,
                position: channel.position,
                permission_overwrites: channel.permission_overwrites,
                name: channel.name,
                topic: channel.topic,
                nsfw: channel.nsfw,
                last_message_id: channel.last_message_id,
                bitrate: channel.bitrate,
                user_limit: channel.user_limit,
                rate_limit_per_user: channel.rate_limit_per_user,
                recipients: channel.recipients,
                icon: channel.icon,
                owner_id: channel.owner_id,
                application_id: channel.application_id,
                parent_id: channel.parent_id,
                last_pin_timestamp: channel.last_pin_timestamp
            };

            await this.channelCache.updateOne({"_id": channel.id}, {$set: this.prepare(strippedChannel)}, {upsert: true});
        }
    }

    public async cacheUser(user: User) {
        if (user) {
            let strippedUser: User = {
                id: user.id,
                username: user.username,
                discriminator: user.discriminator,
                bot: user.bot,
                avatar: user.avatar,
            };

            await this.userCache.updateOne({"_id": user.id}, {$set: this.prepare(strippedUser)}, {upsert: true});
        }
    }

    public async cachePresence(presence) {
        if (presence) {
            let strippedPresence: CachePresence = {
                joined_at: presence.joined_at,
                nick: presence.nick,
                premium_since: presence.premium_since,
                roles: presence.roles,
                status: presence.status,
                deaf: undefined,
                mute: undefined,
                user: undefined,
                guild_id: undefined
            };

            console.log(strippedPresence);

            await this.presenceCache.updateOne({"_id": {g: presence.guild_id, u: presence.user.id}}, {$set: this.prepare(strippedPresence)}, {upsert: true});
        }
    }

    private async deletePresences(guild_id: string) {
        // TODO
    }

    public async cacheGuild(guild: Guild) {
        if (guild) {
            let memberIds = guild.members ? guild.members.map(m => m.user.id) : [];
            let channelIds = guild.channels ? guild.channels.map(m => m.id) : [];

            let strippedGuild: CacheGuild = {
                id: guild.id,
                name: guild.name,
                icon: guild.icon,
                splash: guild.splash,
                owner_id: guild.owner_id,
                permissions: guild.permissions,
                region: guild.region,
                afk_channel_id: guild.afk_channel_id,
                afk_timeout: guild.afk_timeout,
                embed_enabled: guild.embed_enabled,
                embed_channel_id: guild.embed_channel_id,
                verification_level: guild.verification_level,
                default_message_notifications: guild.default_message_notifications,
                explicit_content_filter: guild.explicit_content_filter,
                roles: guild.roles,
                emojis: guild.emojis,
                features: guild.features,
                mfa_level: guild.mfa_level,
                application_id: guild.application_id,
                widget_enabled: guild.widget_enabled,
                widget_channel_id: guild.widget_channel_id,
                system_channel_id: guild.system_channel_id,
                joined_at: guild.joined_at,
                large: guild.large,
                unavailable: guild.unavailable,
                voice_states: guild.voice_states,
                vanity_url_code: guild.vanity_url_code,
                description: guild.description,
                banner: guild.banner,
                premium_tier: guild.premium_tier,
                premium_subscription_count: guild.premium_subscription_count,
                member_ids: memberIds,
                channel_ids: channelIds,
            };

            await this.guildCache.updateOne({"_id": guild.id}, {$set: this.prepare(strippedGuild)}, {upsert: true});

            if (guild.channels) {
                for (const channel of guild.channels) {
                    await this.cacheChannel(channel);
                }
            }

            if (guild.members) {
                for (const member of guild.members) {
                    console.log(member);
                    await this.cacheUser(member.user);
                }
            }

            if (guild.presences) {
                for (let presence of guild.presences) {
                    presence["guild_id"] = guild.id;
                    await this.cachePresence(presence);
                }
            }
        }
    }

    public async deleteGuild(id: string) {
        await this.guildCache.deleteOne({"_id": id});
        await this.deletePresences(id);
    }

    public async getGuild(id: string): Promise<CacheGuild> {
        const guild = await this.guildCache.findOne({"_id": id});
        if (guild) {
            guild.id = guild._id;
            return guild as CacheGuild;
        }
        return null;
    }

    public async addGuildMember(guildId: string, memberId: string) {
        return await this.guildCache.updateOne({"_id": guildId}, {$addToSet: {"member_ids": memberId}});
    }

    public async addGuildMembers(guildId: string, memberIds: string[]) {
        return await this.guildCache.updateOne({"_id": guildId}, {$addToSet: {"member_ids": {$each: memberIds}}});
    }

    public async remGuildMember(guildId: string, memberId: string) {
        return await this.guildCache.updateOne({"_id": guildId}, {$pull: {"member_ids": memberId}});
    }

    public async remGuildMembers(guildId: string, memberIds: string[]) {
        return await this.guildCache.updateOne({"_id": guildId}, {$pull: {"member_ids": {$in: memberIds}}});
    }

}
