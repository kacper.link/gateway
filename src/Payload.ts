import { OpCode } from "./Constants";

export enum StatusType {
    ONLINE = "online",
    DO_NOT_DISTURB = "dnd",
    IDLE = "idle",
    INVISIBLE = "invisible",
    OFFLINE = "offline",
}

export enum ActivityType {
    GAME,
    STEAMING,
    LISTENING,
    WATCHING,
    CUSTOM,
}

export enum ChannelType {
    GUILD_TEXT,
    DM,
    GUILD_VOICE,
    GROUP_DM,
    GUILD_CATEGORY,
    GUILD_NEWS,
    GUILD_STORE,
}

export type Payload = HeartbeatPayload
    | IdentifyPayload
    | ResumePayload
    | HelloPayload
    | HeartbeatACKPayload
    | GenericPayload;

interface HeartbeatPayload extends GenericPayload {
    op: OpCode.HEARTBEAT;
    d: number;
}

interface IdentifyPayload extends GenericPayload {
    op: OpCode.IDENTIFY;
    d: {
        token: string;
        properties: {
            $os: string;
            $browser: string;
            $device: string;
        };
        compress?: boolean;
        large_threshold?: number;
        shard?: [number, number];
        presence?: Presence;
        guild_subscriptions?: boolean;
    };
}

interface ResumePayload extends GenericPayload {
    op: OpCode.RESUME;
    d: {
        token: string;
        session_id: string;
        seq: number;
    };
}

interface HelloPayload extends GenericPayload {
    op: OpCode.HELLO;
    d: { heartbeat_interval: number; };
}

interface HeartbeatACKPayload extends GenericPayload {
    op: OpCode.HEARTBEAT_ACK;
}

interface GenericPayload {
    op: OpCode;
    d?: any;
    t?: string;
    s?: number;
}

export interface Activity {
    name: string;
    type: ActivityType;
    url?: string;
    id?: string;
    created_at?: number;
    timestamps?: { start?: number; end?: number; };
    application_id: string;
    details?: string;
    state?: string;
    emoji?: { name: string; id?: string; animated?: boolean; };
    party?: { id?: string; size?: [number, number] };
    assets?: { large_image?: string; large_text?: string; small_image?: string; small_text?: string; };
    secrets?: { join?: string; spectate?: string; match?: string; };
    instance?: boolean;
    flags?: number;
}

export interface Presence {
    since?: number;
    game?: Activity;
    status: StatusType;
    afk: boolean;
}

export interface User {
    id: string;
    username: string;
    discriminator: string;
    avatar: string;
    bot?: boolean;
    system?: boolean;
    email?: string;
    flags?: number;
    locale?: string;
    premium_type?: number;
    verified?: boolean;
    mfa_enabled?: boolean;
}

export interface Member {
    user: User;
    nick?: string;
    roles: string[];
    joined_at: string;
    premium_since?: string;
    deaf: boolean;
    mute: boolean;
}

export interface Role {
    id: string;
    name: string;
    color: number;
    hoist: boolean;
    position: number;
    permissions: number;
    managed: boolean;
    mentionable: boolean;
}

export interface Emoji {
    id: string;
    name: string;
    roles?: string[];
    user?: User;
    require_colors?: boolean;
    managed?: boolean;
    animated?: boolean;
}

export interface Channel {
    id: string;
    type: ChannelType;
    guild_id?: string;
    position?: number;
    permission_overwrites?: Array<{ id: string; type: string; allow: number; deny: number; }>;
    name?: string;
    topic?: string;
    nsfw?: boolean;
    last_message_id?: string;
    bitrate?: number;
    user_limit?: number;
    rate_limit_per_user?: number;
    recipients?: User[];
    icon?: string;
    owner_id?: string;
    application_id?: string;
    parent_id?: string;
    last_pin_timestamp?: string;
}

export interface VoiceState {
    guild_id?: string;
    channel_id: string;
    user_id: string;
    member?: Member;
    session_id: string;
    deaf: boolean;
    mute: boolean;
    self_deaf: boolean;
    self_mute: boolean;
    self_stream?: boolean;
    suppress: boolean;
}

export interface UnavailableGuild {
    id: string;
    unavailable?: boolean;
}

export interface Guild extends UnavailableGuild {
    id: string;
    name: string;
    icon: string;
    splash: string;
    owner?: boolean; // -- not included in cache
    owner_id: string;
    permissions?: number;
    region: string;
    afk_channel_id: string;
    afk_timeout: number;
    embed_enabled?: boolean;
    embed_channel_id?: string;
    verification_level: number;
    default_message_notifications: number;
    explicit_content_filter: number;
    roles: Role[];
    emojis: Emoji[];
    features: string[];
    mfa_level: number;
    application_id: string;
    widget_enabled?: boolean;
    widget_channel_id?: string;
    system_channel_id: string;
    joined_at?: string;
    large?: boolean;
    unavailable?: boolean;
    member_count?: number; // -- not included in cache
    voice_states?: VoiceState[];
    members?: Member[]; // -- not included in cache
    channels?: Channel[]; // -- not included in cache
    presences?: object[]; // -- not included in cache
    max_presences?: number; // -- not included in cache
    vanity_url_code: string;
    description: string;
    banner: string;
    premium_tier: number;
    premium_subscription_count?: number;
    preferred_locale?: string;
}

export interface ReadyEvent {
    v: number;
    user: User;
    private_channels: string[];
    guilds: UnavailableGuild[];
    session_id: string;
    shard?: [number, number];
    application: { id: string; flags: number };
}
